package application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;

public class ShiftReportXLS {
	protected int rowId;
	protected int cellId;
	protected XSSFWorkbook shiftReportXLS;
	protected String shiftReportFileName;
	protected String shiftReportFilePathFull;
	protected File shiftReportXLSFile;
	protected FileOutputStream shiftReportOutput;
	protected Map<String, String> header;
	protected Map<String,StationDescription> descriptions;
	protected List <String> listOfStations;
	protected String path;
	protected List<String> listOfWorks;
	protected Boolean fileSaved;
	
	
	public ShiftReportXLS(Map<String, String> headerData, Map<String,StationDescription> stationDescriptons, String filePath, HashMap<String,String> MapOfTemp) {
		fileSaved = false;
		header = headerData;
		descriptions = stationDescriptons;
		path = filePath;  //<-eredeti
		
		//path = "C:\\Java\\Muszakatado tesztelgetes\\"; //<-csak prg teszthez, majd ki kell t�r�lni

		listOfStations = new ArrayList<String>();
		for (String element : descriptions.keySet()) {
			listOfStations.add(element);
		}
		
		listOfWorks = new ArrayList<String>();
		listOfWorks = descriptions.get(listOfStations.get(0)).getListOfWorks();
		
		
		shiftReportXLS = new XSSFWorkbook();
		
		CellStyle wordWrapStyle = shiftReportXLS.createCellStyle();
		wordWrapStyle.setWrapText(true);
		
		
		XSSFSheet shiftReportSheet = shiftReportXLS.createSheet(header.get("shiftsDate")+header.get("shiftsTime"));			
			rowId=0;
			//1. sor
			shiftReportSheet.createRow(rowId++).createCell(0).setCellValue("M�szakjelent�s");
			//2. sor
			shiftReportSheet.createRow(rowId++).createCell(0).setCellValue("PPM - IMT");
			//3. sor	
			XSSFRow shiftReportRow3 = shiftReportSheet.createRow(rowId++);
				shiftReportRow3.createCell(0).setCellValue("D�tum:");
				shiftReportRow3.createCell(1).setCellValue(header.get("shiftsDate"));
			//4. sor
			XSSFRow shiftReportRow4 = shiftReportSheet.createRow(rowId++);	
				shiftReportRow4.createCell(0).setCellValue("Id�tartam:");
				shiftReportRow4.createCell(1).setCellValue(header.get("shiftsTime"));
			//5. sor
			XSSFRow shiftReportRow5 = shiftReportSheet.createRow(rowId++);
				shiftReportRow5.createCell(0).setCellValue("M�szak:");
				shiftReportRow5.createCell(1).setCellValue(header.get("shiftsLetter"));
			//6 sor
			XSSFRow shiftReportRow6 = shiftReportSheet.createRow(rowId++);
				shiftReportRow6.createCell(0).setCellValue("K�sz�tette:");
				shiftReportRow6.createCell(1).setCellValue(header.get("techsName"));
			rowId++;
			//7.sor
			
			//8. sor
			XSSFFont fontBold = shiftReportXLS.createFont();
			fontBold.setBold(true);
			XSSFCellStyle styleBold = shiftReportXLS.createCellStyle();
			styleBold.setFont(fontBold);
			
			XSSFCellStyle okStyle = shiftReportXLS.createCellStyle();
			XSSFCellStyle notOkStyle = shiftReportXLS.createCellStyle();
			okStyle.setFillForegroundColor(IndexedColors.LIME.index);
			okStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			notOkStyle.setFillForegroundColor(IndexedColors.RED.index);
			notOkStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			
			for(String element:listOfStations) {
				XSSFRow shiftReportRow7 = shiftReportSheet.createRow(rowId++);
				shiftReportRow7.createCell(0);
				shiftReportRow7.createCell(1);
				shiftReportRow7.createCell(2);
				
				
				String status;
				if (stationDescriptons.get(element).getCbStationIsOk().isSelected()) {
					status = "�llom�s: OK";
					shiftReportRow7.getCell(0).setCellStyle(okStyle);
					shiftReportRow7.getCell(1).setCellStyle(okStyle);
					shiftReportRow7.getCell(2).setCellStyle(okStyle);
					
				}else {
					status = "�llom�s: NotOK";
					shiftReportRow7.getCell(0).setCellStyle(notOkStyle);
					shiftReportRow7.getCell(1).setCellStyle(notOkStyle);
					shiftReportRow7.getCell(2).setCellStyle(notOkStyle);
					
				}
				shiftReportRow7.getCell(0).setCellValue(element);				//<--�llom�s neve
				shiftReportRow7.getCell(1).setCellValue(status);				//<--�llom�s st�tusza
				
				
				

				for(int i =0; i<listOfWorks.size();i++) {
					XSSFRow titleRow = shiftReportSheet.createRow(rowId++);
					titleRow.createCell(1).setCellValue(descriptions.get(element).getDescriptions().get(i).workLabel.getText());
					titleRow.getCell(1).setCellStyle(styleBold);
					XSSFRow workRow = shiftReportSheet.createRow(rowId++);
					workRow.createCell(1).setCellValue(descriptions.get(element).getDescriptions().get(i).getDescription());  //<--�lom�son v�gzett munk�k le�r�sa
					workRow.getCell(1).setCellStyle(wordWrapStyle);
					
				}
				if (!descriptions.get(element).getCbStationIsOk().isSelected()) {
					XSSFRow extraTitleRow= shiftReportSheet.createRow(rowId++);
					extraTitleRow.createCell(1).setCellValue(descriptions.get(element).getLblToDo().getText());
					extraTitleRow.getCell(1).setCellStyle(styleBold);
					XSSFRow extraWorkRow= shiftReportSheet.createRow(rowId++);
					extraWorkRow.createCell(1).setCellValue(descriptions.get(element).getTaWorksToDo().getText());
					extraWorkRow.getCell(1).setCellStyle(wordWrapStyle);
				}
				
				
				shiftReportSheet.createRow(rowId++).createCell(1).setCellValue("Kliens program:");
				shiftReportSheet.getRow(rowId-1).getCell(1).setCellStyle(styleBold);
				
				for (String e : MapOfTemp.keySet()) {
					if (element.contains(e)) {
						String tempOfStation = MapOfTemp.get(e);
						String data[] = tempOfStation.split("\n");
						for (String tempLine : data) {
							cellId=0;
							XSSFRow row = shiftReportSheet.createRow(rowId++);
							row.createCell(cellId++).setCellValue("");
							
							row.createCell(cellId++).setCellValue(tempLine.replace(";"," | "));
							
						}
						
					}
				}
				shiftReportSheet.createRow(rowId++).createCell(1).setCellValue("Cser�lt kop� alkatr�szek:");
				shiftReportSheet.getRow(rowId-1).getCell(1).setCellStyle(styleBold);
				
				boolean replaced = false;
				for (CheckBox cb : descriptions.get(element).getCbsOfWearingPart()) {
					if (cb.isSelected()) {
						shiftReportSheet.createRow(rowId++).createCell(1).setCellValue(cb.getText());
						replaced = true;
					}
				}
				if (!replaced) {
					shiftReportSheet.createRow(rowId++).createCell(1).setCellValue("-nincs-");
				}
				
				XSSFRow blankRow = shiftReportSheet.createRow(rowId++);
				blankRow.createCell(cellId++);
				
				shiftReportSheet.autoSizeColumn(0);
				shiftReportSheet.autoSizeColumn(1);
			}
	}
	public Boolean saveFile() {
		fileSaved = true;
		shiftReportFileName = 	header.get("shiftsDate")+"_"+
								header.get("techsName")+"_"+
								header.get("shiftsLetter")+"_"+
								header.get("shiftsTime")+".xlsx";

		shiftReportFilePathFull = path + shiftReportFileName;
		shiftReportXLSFile = new File(shiftReportFilePathFull);
		try {
			shiftReportOutput = new FileOutputStream (shiftReportXLSFile);
			
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Hiba");
			alert.setHeaderText("A megadott �tvonal nem l�tezik.");
			alert.setContentText(path +"\n\nNem siker�lt menteni a m�szak�tad�t, mert a megadott �tvonal nem l�tezik.");
			alert.showAndWait();
			fileSaved = false;
			return fileSaved;
		}
		try {
			shiftReportXLS.write(shiftReportOutput);
		} catch (IOException e) {
			System.out.println("Nem j� a f�jl!");
			e.printStackTrace();
			return fileSaved;
		}
	return fileSaved;	
	}
}
	
	

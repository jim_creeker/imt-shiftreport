package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ShiftReportTemp {

	protected ObservableList<String> listOfNonReportedStations;
	protected Map <String, StationDescription>  mapOfStationDescriptions;
	protected Map <String, String> mapOfShiftHeader;
	protected String tempFilePath;
	protected File shiftReportTempFile;
	
	
	public ShiftReportTemp(String in_path) {
		tempFilePath = in_path;
	}
	public void write() throws IOException {
		try {
			PrintWriter writer = new PrintWriter(tempFilePath, "UTF-8");
			writer.println("list_of_non_reported_stations");
			writer.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
	} 
	public ObservableList<String> getListOfNonReportedStations() {
		return listOfNonReportedStations;
	}
	public void setListOfNonReportedStations(ObservableList<String> listOfNonReportedStations) {
		this.listOfNonReportedStations = FXCollections.observableArrayList();
		this.listOfNonReportedStations = listOfNonReportedStations;
	}
	
	public Map<String, StationDescription> getMapOfStationDescriptions() {
		return mapOfStationDescriptions;
	}
	public void setMapOfStationDescriptions(Map<String, StationDescription> mapOfStationDescriptions) {
		this.mapOfStationDescriptions = new HashMap<String,StationDescription>();
		this.mapOfStationDescriptions = mapOfStationDescriptions;
	}
	
	public Map<String, String> getMapOfShiftHeader() {
		return mapOfShiftHeader;
	}
	public void setMapOfShiftHeader(Map<String, String> mapOfShiftHeader) {
		this.mapOfShiftHeader = new HashMap <String, String>();
		this.mapOfShiftHeader = mapOfShiftHeader;
	}
	
	
}
package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import javafx.scene.control.TextArea;

public class StationReport {
	private List<String> listOfWorks;	//Ebbe a list�ba mennek feladatok nevei. (Befejezett jav�t�sok, Befejezetlen jav�t�sok stb.)
	private String[] jobsDone;		//Ebbe a t�mbbe mennek a t�nylegesen elv�gzett feladatok ler�sai.
	
	StationReport(String settingsPath) throws FileNotFoundException{
		
		File settingsFile = new File(settingsPath);
		boolean read = false;
		Scanner sc = new Scanner(settingsFile);
		while (sc.hasNext()) {
			if (sc.nextLine().equals("/works")) read = false;
			if (read) {
				listOfWorks.add(sc.nextLine());
			}
			if (sc.nextLine().equals("works")) read = true;
		}
		sc.close();
		jobsDone = new String[listOfWorks.size()];

		
	}
	public List<String> getListOfWorks() {
		return getListOfWorks();
	}
	public String[] getJobsDone() {
		return jobsDone;
	}
	public void setJobReport(int i, String report) {
		jobsDone[i]=report;
	}
	public String getJobReport(int i) {
		return jobsDone[i];
	}
}

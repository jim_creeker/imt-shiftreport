package application;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;


public class StationDescription {
	protected String descriptionName;
	protected Map<Integer,WorkDescriptionArea> MapOfDescriptions; 
	protected List<String> workList;
	protected List<CheckBox> cbsOfWearingPart;
	
	
	protected CheckBox cbStationIsOk = new CheckBox("Az �llom�s haszn�lhat�.");
	protected CheckBox cbEmail = new CheckBox("E-mail �rtes�t�s (Id�ig�nyes lehet. V�rd ki!)");
	protected TextArea taWorksToDo = new TextArea();
	protected Label lblToDo = new Label("Tov�bbi sz�ks�ges munk�k/�tletek ill. tov�bbra is fenn�ll� hib�k:");

	
	
	public StationDescription(String stationName, List<String> listOfWorks, String tempFilePath, LinkedList<String> listOfWearingParts){
		cbStationIsOk.setSelected(true);
		cbEmail.setSelected(false);
		workList = listOfWorks;
		setDescriptionName(stationName);
		MapOfDescriptions = new HashMap<Integer, WorkDescriptionArea>();
		taWorksToDo.setWrapText(true);
		
		
		for (String element : listOfWorks) {
			Integer i = listOfWorks.indexOf(element);
			WorkDescriptionArea work = new WorkDescriptionArea(element);
			MapOfDescriptions.put(i, work);
		}
		
		taWorksToDo.setDisable(true);
		lblToDo.setDisable(true);
		lblToDo.setStyle("-fx-font-weight: bold");
		cbStationIsOk.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
					Boolean newValue) {
				
				if (newValue) {
					taWorksToDo.clear();
					taWorksToDo.setDisable(true);
					lblToDo.setDisable(true);
					cbEmail.setSelected(false);
					cbEmail.setDisable(false);;
				}else {
					
					taWorksToDo.setDisable(false);
					lblToDo.setDisable(false);
					cbEmail.setSelected(true);
					cbEmail.setDisable(true);;
					
				}
			}		
		});
		
		cbsOfWearingPart = new LinkedList<CheckBox>();
		for (String element : listOfWearingParts) {
			CheckBox box = new CheckBox(element);
			cbsOfWearingPart.add(box);
		}
	}
	
	public void setDescriptionName(String s) {
		descriptionName=s;
	}
	public String getDescriptionName() {
		return descriptionName;
	}
	public Map<Integer, WorkDescriptionArea> getDescriptions() {
		return MapOfDescriptions;
	}
	public List<String> getListOfWorks(){
		return workList;
	}

	public CheckBox getCbStationIsOk() {
		return cbStationIsOk;
	}

	public void setCbStationIsOk(CheckBox cbStationIsOk) {
		this.cbStationIsOk = cbStationIsOk;
		
	}

	public TextArea getTaWorksToDo() {
		return taWorksToDo;
	}

	public void setTaWorksToDo(TextArea taWorksToDo) {
		this.taWorksToDo = taWorksToDo;
	}

	public Label getLblToDo() {
		return lblToDo;
	}
	public List<CheckBox> getCbsOfWearingPart() {
		return cbsOfWearingPart;
	}
	public CheckBox getCbEmail() {
		return cbEmail;
	}


}

package application;

import javafx.scene.control.Label;
import javafx.scene.control.TextArea;


public class WorkDescriptionArea {
	
	protected TextArea workDescription;
	protected Label workLabel;
	
	public WorkDescriptionArea(String work){
		workDescription = new TextArea();
		workDescription.setWrapText(true);
		workLabel = new Label(work);
		workLabel.setStyle("-fx-font-weight: bold");
	}
	public String getWorkName() {
		return workLabel.getText();
	}
	
	public String getDescription() {
		return workDescription.getText();
	}
	
	public Label getWorkLabel() {
		return workLabel;
	}
	
	public TextArea getDescriptionArea() {
		return workDescription;
	}
	public TextArea getWorkDescription() {
		return workDescription;
	}
	public void setWorkDescription(TextArea workDescription) {
		this.workDescription = workDescription;
	}	
}


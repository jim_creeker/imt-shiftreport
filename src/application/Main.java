package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("M�szakjelent� v2.0");
			Parent root = FXMLLoader.load(getClass().getResource("shiftReport_2.fxml"));
			
			Scene scene = new Scene(root,1680,925);
			primaryStage.setScene(scene);
			primaryStage.setMaximized(true);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}


package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;

public class stationHistoryXLS {
	protected HSSFWorkbook logFileXLS;
	protected HSSFSheet Sheet;
	protected HSSFRow newRow;
	protected HSSFRow rowBefore;
	protected HSSFRow rowAfter;
	protected String path;
	protected Map<String,String> header;
	protected StationDescription newContent;
	protected FileInputStream input;
	protected File logFile;
	protected Boolean fileSaved;

	public stationHistoryXLS(String filePath, StationDescription description, Map<String, String> shiftHeader) {
		fileSaved = false;
		header = new HashMap<String,String>();
		header = shiftHeader;
		path = filePath;
		
		//path = "C:\\Java\\Muszakatado tesztelgetes\\Atmeneti_beavatkoz�si napl�.xls";  //<-csak prg teszthez, majdk i kell t�r�lni
		
		newContent = description;
		
		
		
		try {
			logFile = new File(path);
			input = new FileInputStream(logFile);
			try {
				logFileXLS = new HSSFWorkbook(input);
				CellStyle wordWrapStyle = logFileXLS.createCellStyle();
				wordWrapStyle.setWrapText(true);
				
				Sheet = logFileXLS.getSheetAt(0);
				rowBefore=Sheet.getRow(4);
				rowAfter=Sheet.getRow(5);
				
				Sheet.createRow(4);
					for (CheckBox cb : newContent.getCbsOfWearingPart()) {
						if (cb.isSelected()) {
							Sheet.shiftRows(31, Sheet.getLastRowNum(), 1,true,true);
							Sheet.createRow(31);
							int cellID = 0;
							Sheet.getRow(31).createCell(cellID++).setCellValue(Sheet.getRow(32).getCell(0).getNumericCellValue()+1);
							Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsDate"));
							Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsTime"));
							Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("techsName"));
							Sheet.getRow(31).createCell(cellID++).setCellValue("M�szakjelent�");
							Sheet.getRow(31).createCell(cellID++).setCellValue("Cser�lt kop� alkatr�sz:");
							Sheet.getRow(31).createCell(cellID++).setCellValue(cb.getText());
						}
					}
					if (!newContent.getCbStationIsOk().isSelected()) {
						Sheet.shiftRows(31, Sheet.getLastRowNum(), 1,true,true);
						Sheet.createRow(31);
						int cellID = 0;
						Sheet.getRow(31).createCell(cellID++).setCellValue(Sheet.getRow(32).getCell(0).getNumericCellValue()+1);
						Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsDate"));
						Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsTime"));
						Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("techsName"));
						Sheet.getRow(31).createCell(cellID++).setCellValue("M�szakjelent�");
						
						Sheet.getRow(31).createCell(cellID++).setCellValue(newContent.getLblToDo().getText());
						Sheet.getRow(31).createCell(cellID++).setCellValue(newContent.getTaWorksToDo().getText());
						Sheet.getRow(31).getCell(cellID-1).setCellStyle(wordWrapStyle);
						
						
					}
					for (String element : description.getListOfWorks()) {
						int i = description.getListOfWorks().indexOf(element);
						if (newContent.getDescriptions().get(i).getDescription().length()>0) {				
							Sheet.shiftRows(31, Sheet.getLastRowNum(), 1,true,true);
							Sheet.createRow(31);
							int cellID = 0;
							Sheet.getRow(31).createCell(cellID++).setCellValue(Sheet.getRow(32).getCell(0).getNumericCellValue()+1);
							Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsDate"));
							Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsTime"));
							Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("techsName"));
							Sheet.getRow(31).createCell(cellID++).setCellValue("M�szakjelent�");
							
							
							Sheet.getRow(31).createCell(cellID++).setCellValue(newContent.getDescriptions().get(i).getWorkLabel().getText());
							Sheet.getRow(31).createCell(cellID++).setCellValue(newContent.getDescriptions().get(i).getDescription());
							Sheet.getRow(31).getCell(cellID-1).setCellStyle(wordWrapStyle);
						}
					}
					
					Sheet.shiftRows(31, Sheet.getLastRowNum(), 1,true,true);
					Sheet.createRow(31);
					int cellID = 0;
					Sheet.getRow(31).createCell(cellID++).setCellValue(Sheet.getRow(32).getCell(0).getNumericCellValue()+1);
					Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsDate"));
					Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("shiftsTime"));
					Sheet.getRow(31).createCell(cellID++).setCellValue(header.get("techsName"));
					Sheet.getRow(31).createCell(cellID++).setCellValue("M�szakjelent�");
					String status;
					if (newContent.getCbStationIsOk().isSelected()) {
						status = "OK. (Haszn�lhat�/haszn�latban van)";
					}else {
						status = "NotOK. (Nem haszn�lhat�)";
					}
					Sheet.getRow(31).createCell(cellID++).setCellValue("�llom�s st�tusza:");
					Sheet.getRow(31).createCell(cellID++).setCellValue(status);
					
					try {
						input.close();
					} catch (IOException e) {}
			} catch (Exception e) {
				//nem j� f�jl
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Hiba");
				alert.setHeaderText("C�lf�jl tpusa nem megfelel�!");
				alert.setContentText(path +"\n\nA megadott f�jl nem .xls Excel f�jl.");
				alert.showAndWait();
			}				
			
		} catch (FileNotFoundException e) {
			//f�jl nincs meg
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Hiba");
			alert.setHeaderText("Nincs meg a f�jl!");
			alert.setContentText(path +"\n\nLehets�ges okok:\n - F�jl nincs a hely�n vagy �t lett nevezve\n - F�jl helye nem el�rhet�");
			alert.showAndWait();
		}				
				
	}	
	public boolean save() {
		if(logFile.exists()) {
			fileSaved=true;
			FileOutputStream output;
			try {
				output = new FileOutputStream(logFile);
				try {
					logFileXLS.write(output);
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}			
			} catch (FileNotFoundException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Hiba");
				alert.setHeaderText("C�l f�jl haszn�latban van!");
				alert.setContentText(path +"\n\nA megadott f�jl valakin�l meg van nyiva. Bejegyz�s nem t�rt�nt.");
				alert.showAndWait();
				fileSaved=false;
			}
		} else {
			fileSaved=false;
		}
	
	return fileSaved;
	}
}

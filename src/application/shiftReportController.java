package application;


import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import javax.mail.Message;
import javax.mail.MessagingException;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class shiftReportController implements Initializable {
//FXML-es objectumok	
	@FXML VBox vBoxForDescriptions;
	@FXML Label lblStationName = new Label();
	@FXML Label lblAllStations = new Label();
	@FXML Label lblReportedStations = new Label();
	@FXML Button btnAddStation= new Button();
	@FXML Button btnRemStation = new Button();
	@FXML Button btnOpenShift = new Button();
	@FXML Button btnCloseShift = new Button();
	@FXML ListView<String> listViewOfAllStations = new ListView<String>();
	@FXML ListView<String> listViewOfStationsToReport;
	@FXML ChoiceBox<String> cbShiftLetter = new ChoiceBox<String>();
	@FXML ChoiceBox<String> cbShiftTime = new ChoiceBox<String>();
	@FXML TextField tfTechNAme = new TextField();
	@FXML DatePicker dpDatePicker = new DatePicker();
	@FXML Separator separator;
	
	
	protected String shiftReportTemp;
	protected List<String> tempContent;
	protected HashMap<String,String> MapOfTemp = new HashMap<String,String>();
	protected String mailTo, mailSubject, mailContent;
	
	
	
	

//Tov�bbi v�ltoz�k, objektumok	
	final String FOLDER_OF_STATIONS = "stations";
	final String SETTINGS_FILE_PATH = "settings.txt";
	String shiftReportXLSPath;
	List<String> listOfWorks = new ArrayList<String>();
	List<String> stationWithReplacedParts;
	
	ObservableList<String> listOfNonReportedStations = FXCollections.observableArrayList();
	ObservableList<String> listOfReportedStations = FXCollections.observableArrayList();
	ObservableList<String> listOfShiftLetters = FXCollections.observableArrayList();
	ObservableList<String> listOfShiftTimes = FXCollections.observableArrayList();

	Map <String, StationDescription>  mapOfStationDescriptions = new HashMap<String,StationDescription>();
	Map<String,String> mapOfTechs = new HashMap<String,String>();
	Map <String, String> mapOfShiftHeader = new HashMap <String, String>();
	Map<String,String> mapOfLogFielPath = new HashMap<String,String>();
	Map<String,LinkedList<String>> mapOfWearingParts = new HashMap<String,LinkedList<String>>();
	Map<String,String> mapOfWearPartPath = new HashMap<String,String>();
	
//--------------Scene eberendez�se---------------------------------------------------------------------------------------	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		readSettings();
		
		readStations(FOLDER_OF_STATIONS);
		
		readTemp();
		
		setScene();

		
//-----------Kezel� szervek---------------------------------------------------------------------------------------------------------	
		
//M�szakv�laszt� m�k�d�se		
		cbShiftLetter.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tfTechNAme.setText(mapOfTechs.get(cbShiftLetter.getValue()));	
			}
		});

//M�szaknyit� gomb m�k�d�se	
		btnOpenShift.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				mapOfShiftHeader.put("shiftsLetter", cbShiftLetter.getValue());
				mapOfShiftHeader.put("techsName",tfTechNAme.getText());
				mapOfShiftHeader.put("shiftsDate", dpDatePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy.MM.dd.")));
				mapOfShiftHeader.put("shiftsTime", cbShiftTime.getValue());

				boolean allSet = true;
				
				if (cbShiftLetter.getValue()==null)					allSet = false;
				if (tfTechNAme.getText().length()==0)				allSet = false;
				if (tfTechNAme.getText().equals("Nin�cska (L)"))	allSet = false;
				if (cbShiftTime.getValue()==null)					allSet = false;
				if (dpDatePicker.getValue()==null)					allSet = false;
				
				if(allSet) {
					lblAllStations.setVisible(true);
					lblReportedStations.setVisible(true);
					listViewOfAllStations.setVisible(true);
					listViewOfStationsToReport.setVisible(true);
					btnAddStation.setVisible(true);
					btnRemStation.setVisible(true);
					btnCloseShift.setVisible(true);
					separator.setVisible(true);
					
					cbShiftLetter.setDisable(true);
					tfTechNAme.setDisable(true);
					cbShiftTime.setDisable(true);
					dpDatePicker.setDisable(true);
					btnOpenShift.setDisable(true);
				} else {
					if (dpDatePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy.MM.dd.")).contentEquals("1991.03.02.")) {
						tfTechNAme.setText("Nin�cska (L)");
					}
				}
				Set<String> coll = MapOfTemp.keySet();
				TreeSet<String> treeSet = new TreeSet<String>();
				treeSet.addAll(coll);
				
				for (String stationInTemp : treeSet) {
					for (String stationToReport : listOfNonReportedStations) {
						if (stationToReport.contains(stationInTemp)) {
							addStationReport(stationToReport);
							break;
						}
					}
				}
				
				
			}
			
		});

//�sszes �llom�s Listview m�k�d�se
		this.listViewOfAllStations.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if(event.getButton().equals(MouseButton.PRIMARY)){
		            if(event.getClickCount() == 2){
		            	addStationReport();
		            }
				}
			}});
		
//Hozz�ad� gomb m�k�d�se
		this.btnAddStation.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				addStationReport();
			}
		});
		
//Kivev� gomb m�k�d�se
		this.btnRemStation.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				removeStationReport();
			}
		});
		
//Itt van lekezelve, mi t�rt�njen, ha m�sik �llom�st v�lasztunk.
		listViewOfStationsToReport.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				vBoxForDescriptions.getChildren().clear();
				lblStationName.setText("");
				lblStationName.setVisible(false);
				
				if (!(newValue == null)) {
					vBoxForDescriptions.getChildren().clear();
					lblStationName.setText(newValue);
					lblStationName.setVisible(true);
					
					String selectedStation = newValue;
					
					Label lblTempTitle = new Label("M�szak sor�n lejelentett esem�nyek:");
					Label lblTempContent = new Label();
					
					lblTempTitle.setStyle("-fx-font-weight: bold");
					
					for (String element : MapOfTemp.keySet()) {
						if (newValue.contains(element)) {
							lblTempContent.setText(MapOfTemp.get(element).replace(";"," | "));
						}
					}
					
					Button btnShowTemp = new Button("Megn�z");
					btnShowTemp.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							Stage stage = new Stage();
							stage.setTitle("M�szak sor�n lejelentett beavatkoz�sok");
							VBox vbox = new VBox();
							Label lblStation = new Label(lblStationName.getText());
							
							vbox.getChildren().addAll( lblStation,lblTempContent);
							vbox.setPadding(new Insets(20));
							vbox.setSpacing(20);
							
							Scene scene = new Scene(vbox);
							stage.setScene(scene);
							
							stage.show();
							
						}
					});
					
					
					
					HBox hBoxForTemp = new HBox();
					hBoxForTemp.setSpacing(20);
					hBoxForTemp.setAlignment(Pos.CENTER_LEFT);
					hBoxForTemp.getChildren().addAll(lblTempTitle,btnShowTemp);
					
					vBoxForDescriptions.getChildren().add(hBoxForTemp);
					List<CheckBox> cbsList =  mapOfStationDescriptions.get(newValue).getCbsOfWearingPart();
					
					HBox hboxForCBS = new HBox();
					hboxForCBS.setSpacing(20);
					int cols = cbsList.size()/10 +1;
					for (int i =0; i<cols;i++) {
						VBox vbox = new VBox();
						hboxForCBS.getChildren().add(vbox);
					}
					
					for (CheckBox element : cbsList) {
						int c = cbsList.indexOf(element)/10;
						
						VBox actCol = (VBox) hboxForCBS.getChildren().get(c);
						actCol.getChildren().add(element);
					}
					Label lblWearingParts = new Label("Cser�lt kop� alkatr�szek:");
					lblWearingParts.setStyle("-fx-font-weight: bold");
					vBoxForDescriptions.getChildren().addAll(lblWearingParts,hboxForCBS);
					
					
					
					
					if (mapOfStationDescriptions.size()>0) {
						for (String element : listOfWorks) {
							int i = listOfWorks.indexOf(element);
							vBoxForDescriptions.getChildren().add(mapOfStationDescriptions.get(newValue).getDescriptions().get(i).getWorkLabel());
							vBoxForDescriptions.getChildren().add(mapOfStationDescriptions.get(newValue).getDescriptions().get(i).getDescriptionArea());
						}
						
					}
					
					CheckBox cbStationIsOk = mapOfStationDescriptions.get(newValue).getCbStationIsOk();
					cbStationIsOk.setContentDisplay(ContentDisplay.LEFT);
					CheckBox cbSendEmail = mapOfStationDescriptions.get(newValue).getCbEmail();
					HBox hbox = new HBox();
					hbox.setSpacing(20);
					hbox.setMargin(cbStationIsOk, new Insets(10));
					hbox.setMargin(cbSendEmail, new Insets(10));
					hbox.getChildren().addAll(new Label("�llom�s st�tusza:"),cbStationIsOk,cbSendEmail);
					vBoxForDescriptions.getChildren().add(hbox);
					
					vBoxForDescriptions.getChildren().add(mapOfStationDescriptions.get(newValue).getLblToDo());
					
					TextArea stationToDo = mapOfStationDescriptions.get(selectedStation).getTaWorksToDo();
					
					vBoxForDescriptions.getChildren().add(stationToDo);
					
					
					for (Node element:vBoxForDescriptions.getChildren()) {
						vBoxForDescriptions.setMargin(element, new Insets(10));
					}
					
					
					
					
				}
					
				}
				
		});
		
//M�szakz�r� gom m�k�d�se		
		btnCloseShift.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					closeShift();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		});
	}
	
//--------Egy�b methodok-----------------------------------------------------------------------------------------------------------------------	
	
	protected boolean makeWearPartLog(String station, String path) {
		
		try {
			File file = new File (path);
			Scanner sc = new Scanner (file,"UTF8");
			List<String> content = new LinkedList<String>();
			while (sc.hasNextLine()) {
				content.add(sc.nextLine());
			}
			sc.close();
			
			StationDescription description = mapOfStationDescriptions.get(station);
			for (CheckBox cb : description.getCbsOfWearingPart()) {
				if (cb.isSelected()) {
					String newLine = dpDatePicker.getValue() + ";"
							+ cbShiftTime.getValue() + ";"
							+ tfTechNAme.getText() + ";"
							+ cb.getText(); 
					 content.add(0, newLine);
				}
			}
			
			
			try {
				PrintWriter pw;
				pw = new PrintWriter(file,"UTF8");
				for (String element : content) {
					pw.println(element);
				}
				pw.close();
			} catch (UnsupportedEncodingException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Hiba");
				alert.setHeaderText("Hi�nyz� f�jl: "+station);
				alert.setContentText("Az �llom�s kop� alkatr�szeinek cser�j�r�l sz�l� f�jl nem �rhat�.\nItt kerestem:\n"+path+"\n\nBejegyz�s nem k�sz�lt!");
				alert.showAndWait();
			}
			
			
			
			
			
			
			return true;
		} catch (FileNotFoundException e) {
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Hiba");
			alert.setHeaderText("Hi�nyz� f�jl: "+station);
			alert.setContentText("Az �llom�s kop� alkatr�szeinek cser�j�r�l sz�l� f�jl nem nyithat� meg.\nItt kerestem:\n"+path+"\n\nBejegyz�s nem k�sz�lt!");
			alert.showAndWait();
			
			
			
			e.printStackTrace();
			return false;
		}
		
		
		
		
		
		
	}
	

	protected void readSettings() {
		try {
			//Be�ll�t�sok beolvas�sa
						
						listOfWorks.clear();
						listOfShiftLetters.clear();
						mapOfTechs.clear();
						listOfShiftTimes.clear();
						
						File settingsFile = new File (SETTINGS_FILE_PATH);
						Scanner sc = new Scanner(settingsFile);
						//Boolean readStations = false;
						Boolean readWorks = false;
						Boolean readShifts = false;
						Boolean readShiftTimes = false;
						Boolean readMail = false;
						//Boolean readLogPath = false;
						String singleLine;
						
						while(sc.hasNextLine()) {
							singleLine = sc.nextLine();
							if (singleLine.startsWith("shiftReportXLSPath=")) shiftReportXLSPath = singleLine.substring(19);
							if (singleLine.startsWith("shiftReportTemp=")) setShiftReportTemp(singleLine.substring(16));
							
							if (singleLine.equals("/works"))		readWorks = false;
							if (readWorks)							listOfWorks.add(singleLine);
							if (singleLine.equals("works")) 		readWorks = true;
							
							if (singleLine.equals("/shifts")) 		readShifts = false;
							if (readShifts)	{
																	String letter = Character.toString(singleLine.charAt(0));
																	String techname = singleLine.substring(2);
																	listOfShiftLetters.add(Character.toString(singleLine.charAt(0)));
																	mapOfTechs.put(letter, techname);
							}
							if (singleLine.equals("shifts"))		readShifts = true;

							if (singleLine.equals("/shiftstime"))	readShiftTimes = false;
							if (readShiftTimes)						listOfShiftTimes.add(singleLine);
							if (singleLine.equals("shiftstime"))	readShiftTimes = true;
							
							if (singleLine.equals("/email"))	readMail = false;
							if (readMail) {
								String data[] = singleLine.split(";");
								if (data[0].contentEquals("to")) setMailTo(singleLine.replace("to;", ""));
								if (data[0].contentEquals("subject")) setMailSubject(data[1]);
								//if (data[0].contentEquals("message")) setMailContent(data[1]);
							}
							if (singleLine.equals("email"))	readMail = true;
							
							
						}
						sc.close();
					} catch (FileNotFoundException e)	{System.out.println("File not found.");}
	}
	
	protected boolean readStations(String folderPath) {
		listOfNonReportedStations.clear();
		mapOfLogFielPath.clear();
		mapOfWearingParts.clear();
		
		File folderOfStations = new File (folderPath);
		File[] listOfFiles = folderOfStations.listFiles();
		
		for (File element:listOfFiles) {
			String stationName = element.getName().replace(".txt", "");
			listOfNonReportedStations.add(stationName);
			LinkedList<String> list = new LinkedList<String>();
			try {
				Scanner sc = new Scanner(element,"UTF8");
				boolean readWearingParts = false;
				while (sc.hasNextLine()) {
					String data[] = sc.nextLine().split(";");
					if (data[0].contentEquals("log")) mapOfLogFielPath.put(stationName,data[1]);
					if (data[0].contentEquals("wearingPartsLog")) mapOfWearPartPath.put(stationName,data[1]);
					if (data[0].contentEquals("[/WearingParts]")) {
						readWearingParts = false;
					}
					if (readWearingParts) {
						list.add(data[0]);
					}
					if (data[0].contentEquals("[WearingParts]")) {
						readWearingParts = true;
					}
				}
				sc.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mapOfWearingParts.put(stationName, list);
			
		}
		
		return true;
	}
		
	protected void setScene() {
		//El�k�sz�t�s
				lblStationName.setVisible(false);
				lblAllStations.setVisible(false);
				lblReportedStations.setVisible(false);
				listViewOfAllStations.setVisible(false);
				listViewOfStationsToReport.setVisible(false);
				btnAddStation.setVisible(false);
				btnRemStation.setVisible(false);
				btnCloseShift.setVisible(false);
				separator.setVisible(false);

				listViewOfAllStations.setItems(listOfNonReportedStations);
				cbShiftTime.setItems(listOfShiftTimes);
				cbShiftLetter.setItems(listOfShiftLetters);
	}
			
	protected boolean readTemp() {
		MapOfTemp.clear();
		stationWithReplacedParts = new ArrayList<String>();
		try {
			File file = new File (getShiftReportTemp());
			Scanner sc = new Scanner(file, "UTF8");
			//Scanner sc = new Scanner(file);
			//boolean firstLine = true;
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				/*
				if (firstLine) {
					//BOM elt�vol�t�sa
					line = line.substring(1);
					firstLine = false;
				}
				*/
				
				String data[] = line.split(";");
				
				if (data[1].contentEquals("Alkatresz csere") && !(stationWithReplacedParts.contains(data[0]))) {
					stationWithReplacedParts.add(data[0]);
				}
				
				System.out.println(data[0]);
				if (MapOfTemp.keySet().contains(data[0])) {
					String contentToAdd = line.replace(data[0]+";", "");
					String newContent = MapOfTemp.get(data[0]) + "\n" + contentToAdd;
					MapOfTemp.put(data[0], newContent);
					
				} else {
					String contentToAdd = line.replace(data[0]+";", "");				
					MapOfTemp.put(data[0], contentToAdd);
				}

			}
			
			sc.close();
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Figyelmeztet�s");
			alert.setHeaderText("Hi�nyz� f�jl.");
			alert.setContentText("A m�szak sor�n lejelentett beavatkoz�sokat tartalmaz� f�jlt nem siker�lt olvasni. A m�szak�tad�s ett�l m�g megt�rt�nhet, de ezeket az inf�kat manu�lisan kell bevinni.\n\nItt kerestem:\n" + getShiftReportTemp());
			alert.showAndWait();
			e.printStackTrace();
			return false;
		}
		
	}
	
	protected void removeStationReport() {
		for (int i = 0;i<listOfReportedStations.size();i++) {
			if(listViewOfStationsToReport.getSelectionModel().isSelected(i)) {
				String stationName = listOfReportedStations.remove(i);
				listOfNonReportedStations.add(stationName);
				mapOfStationDescriptions.remove(stationName);
				//i--;
			}
			listViewOfAllStations.setItems(listOfNonReportedStations);
			listViewOfStationsToReport.setItems(listOfReportedStations);
		}
	}
	
	protected void addStationReport() {
		for (int i = 0; i < listOfNonReportedStations.size();i++){
			if (listViewOfAllStations.getSelectionModel().isSelected(i)) {
				String stationName;
				stationName = listOfNonReportedStations.remove(i);
				listOfReportedStations.add(stationName);
				LinkedList<String> list = mapOfWearingParts.get(stationName);
				StationDescription stationDesc = new StationDescription(stationName,listOfWorks,getShiftReportTemp(),list);
				mapOfStationDescriptions.put(stationName, stationDesc);
				
			}
			listViewOfAllStations.setItems(listOfNonReportedStations);
			listViewOfStationsToReport.setItems(listOfReportedStations);
		}
	}
	
	protected void addStationReport(String stationName) {
			
		
			listOfNonReportedStations.remove(stationName);	
			listOfReportedStations.add(stationName);
			LinkedList<String> list = mapOfWearingParts.get(stationName);
			
			StationDescription stationDesc = new StationDescription(stationName,listOfWorks,getShiftReportTemp(),list);
			mapOfStationDescriptions.put(stationName, stationDesc);

			listViewOfAllStations.setItems(listOfNonReportedStations);
			listViewOfStationsToReport.setItems(listOfReportedStations);
			
			listViewOfStationsToReport.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
	            @Override
	            public ListCell<String> call(ListView<String> param) {
	                return new ListCell<String>() {
	                    @Override
	                    protected void updateItem(String item, boolean empty) {
	                        super.updateItem(item, empty);

	                        if (item == null || empty) {
	                            setText(null);	                            
	                            setStyle(" -fx-font-weight: normal;");
	                            
	                        } else {
	                            setText(item);
	                            if (stationWithReplacedParts.contains(item)) {	                               
	                            	setStyle(" -fx-font-weight: bolder;");
	                            } else {
	                            	setStyle(" -fx-font-weight: normal;");
	                            }
	                        }
	                    }
	                };
	            }
	        });
			
	}
	
	protected void closeShift() throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Figyelmeztet�s");
		alert.setHeaderText("M�szak z�r�sa");
		alert.setContentText("Ha z�rod a m�szakot:\n\n" 
				+ "- A m�szakban t�rt�nteket le�r�s�t m�r csak a most gener�lt Excel t�bl�k egyes�vel t�rt�n� m�dos�t�s�val m�dos�thatod.\n\n"
				+ "- A m�szak sor�n lejelentett cser�lt alkatr�szeket m�r NEM fogod tudni automatikusan levonni a k�szletb�l. Ezt m�g megteheted, am�g nem kattintasz az 'Igen'.\n\n"
				+"Biztos z�rod a m�szakot?");

		ButtonType btnTypeYes = new ButtonType("Igen");
		ButtonType btnTypeNo = new ButtonType("Nem");
		
		alert.getButtonTypes().setAll(btnTypeYes, btnTypeNo);

		Optional<ButtonType> result = alert.showAndWait();
		
		if (result.get() == btnTypeYes){
			ShiftReportXLS shiftReport = new ShiftReportXLS(mapOfShiftHeader,mapOfStationDescriptions,shiftReportXLSPath,MapOfTemp);
			if (shiftReport.saveFile()) {
				for (String element : listOfReportedStations) {
					makeWearPartLog(element, mapOfWearPartPath.get(element));
					
				}
				for (String element : listOfReportedStations) {
					stationHistoryXLS log = new stationHistoryXLS(mapOfLogFielPath.get(element),mapOfStationDescriptions.get(element),mapOfShiftHeader);
					if (!log.save()){
						Alert stationNotLogged = new Alert(AlertType.ERROR);
						stationNotLogged.setTitle("Figyelmeztet�s");
						stationNotLogged.setHeaderText("Napl�f�jl nem friss�lt.");
						stationNotLogged.setContentText(element +"\n\nEnnek az �llom�snak a beavatkoz�si napl�j�ba nem siker�lt bejegyz�st k�szteni.\n�gy ezt manu�lisan kell p�tolni!");
						stationNotLogged.showAndWait();
						
					}
					
					if (mapOfStationDescriptions.get(element).getCbEmail().isSelected()) {
						sendMail(element, getStationStatus(mapOfStationDescriptions.get(element),MapOfTemp));
					}
					
					listOfNonReportedStations.add(element);
					mapOfStationDescriptions.remove(element);
					
				}
				
				
				

				listViewOfAllStations.setItems(listOfNonReportedStations);
				listOfReportedStations.clear();
				listViewOfStationsToReport.setItems(listOfReportedStations);
				
				lblAllStations.setVisible(false);
				lblReportedStations.setVisible(false);
				listViewOfAllStations.setVisible(false);
				listViewOfStationsToReport.setVisible(false);
				btnAddStation.setVisible(false);
				btnRemStation.setVisible(false);
				btnCloseShift.setVisible(false);
				separator.setVisible(false);
				
				cbShiftLetter.setValue(null);
				tfTechNAme.setText(null);
				cbShiftTime.setValue(null);
				dpDatePicker.setValue(null);
				
				cbShiftLetter.setDisable(false);
				tfTechNAme.setDisable(false);
				cbShiftTime.setDisable(false);
				dpDatePicker.setDisable(false);
				btnOpenShift.setDisable(false);
				
				
				
				
				if (!clearTempFile()) {
					Alert shiftNotClosed = new Alert(AlertType.ERROR);
					shiftNotClosed.setTitle("Hiba");
					shiftNotClosed.setContentText("Nem siker�lt t�t�lni az �tmeneti f�jl tartalm�t, mely a m�szak sor�n lejelentett esem�nyeket tartalmazza.\n"+
													"K�rlek t�r�ld ki manu�lisan a lenti f�jl tartalm�t. Ne a f�jlt! Csak a tartalm�t. Legyen �res!\n\n"+
													getShiftReportTemp());
					shiftNotClosed.show();
				}
				readSettings();
				
				readStations(FOLDER_OF_STATIONS);
				
				readTemp();
				
				setScene();
				
			} else {
				Alert shiftNotClosed = new Alert(AlertType.ERROR);
				shiftNotClosed.setTitle("Hiba");
				shiftNotClosed.setHeaderText("M�szak nem lett lez�rva.");
				shiftNotClosed.show();
			}	
			
		} 		
	}

	protected String getShiftReportTemp() {
		return shiftReportTemp;
	}

	protected void setShiftReportTemp(String shiftReportTemp) {
		this.shiftReportTemp = shiftReportTemp;
	}

	
	protected String getMailTo() {
		return mailTo;
	}

	
	protected void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	
	protected String getMailSubject() {
		return mailSubject;
	}

	
	protected void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	protected String getMailContent() {
		return mailContent;
	}

	protected void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
	
	protected boolean sendMail(String station, String stationStatus) {
		 try {
	            String[] addressArray = getMailTo().split(";");
			 	//String to=addressArray;
	            //String from=MAIL_FROM;

	            Properties props = new Properties();

	            props.put("mail.smtp.host", "mail-relay.linamar.hu");
	            props.put("mail.smtp.port", "25");
	            props.put("mail.smtp.starttls.enable","false");
	            props.put("mail.smtp.auth", "false");
	            
	            
	            Session session = Session.getDefaultInstance(props);
	            /*
	            Session session = Session.getDefaultInstance(props,
	                    new javax.mail.Authenticator() {
	                        protected PasswordAuthentication getPasswordAuthentication() {
	                        	return new PasswordAuthentication(MAIL_USERNAME,MAIL_PASSWORD);
	                        }
	                    });
	           */
	            String msgBody = "Tisztelt C�mzettek!\n\nEzt a levelet a PPM-IMT M�szak�tad� program automatikusan gener�lta. A lev�lre ne v�laszoljanak, tartalm�val kapcsolatban keress�k a m�r�g�pm�szer�szeket.\n\n" + stationStatus;

	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress("NoReply", "PPM-IMT: ShiftReporter"));
	            
	            for (String address:addressArray) {
	            	msg.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
	            }
	            
	            msg.setSubject(getMailSubject()+ " " +station);
	            msg.setText(msgBody);
	            Transport.send(msg);
	            
	            Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("E-mail elk�ldve");
				alert.setHeaderText("");
				String addresses = getMailTo().replace(";","\n");
				alert.setContentText("A "+ station +" �llom�sr�l sz�l� jelent�s sikeresen elk�ldve az al�bbi c�mzetteknek:\n\n"+addresses);
				alert.showAndWait();
	            return true;

	        } catch (AddressException e) {
	        	e.printStackTrace();
	        	Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("E-mail hiba");
				alert.setHeaderText("Hiba a c�mzettekkel.");
				alert.setContentText("C�mzettek:\n" + getMailTo()+"\n\n" + e.getMessage());
				alert.showAndWait();
	        	return false;
	        } catch (MessagingException e) {
	        	Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("E-mail hiba");
				alert.setHeaderText("Hiba az �zenettel.");
				alert.setContentText("C�mzettek:\n" + getMailTo()+"\nT�rgy: "+getMailSubject()+"\n" + e.getMessage());
				alert.showAndWait();
	        	e.printStackTrace();
	        	return false;
	        } catch (UnsupportedEncodingException e) {
	        	e.printStackTrace();
	        	Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("E-mail hiba");
				alert.setHeaderText("Ismeretlen hiba.");
				alert.setContentText(e.getMessage());
				alert.showAndWait();
	        	return false;
	        }
		 
		}
	
	protected String getStationStatus(StationDescription stationDescription, Map<String,String> stationTemp) {
		String status ="";
		
		String stationName = stationDescription.getDescriptionName();
		;
		
		String stationStatus;
		if (stationDescription.getCbStationIsOk().isSelected()) {
			stationStatus = "Az �llom�s haszn�lhat�/haszn�latban van.";
		}else {
			stationStatus = "Az �llom�s NEM haszn�lhat�!";
		}
		
		String temp = "\tM�szak sor�n jelentett esem�nyek:\n";
		for (String element: stationTemp.keySet()) {
			if (stationName.contains(element)) {
				String data[] = stationTemp.get(element).split("\n");
				for (String line : data) {
					temp += "\t\t - " + line.replace(";", " | ") + "\n";
				}
				
			}
		}

		
		String replacedWearingParts ="\tCser�lt kop� alkatr�szek:\n";
		for (CheckBox cb: stationDescription.getCbsOfWearingPart()) {
			if (cb.isSelected()) {
				replacedWearingParts +="\t\t - "+ cb.getText()+ "\n";
			}
		}

		
		String worksDone="";
		
		for (String element : listOfWorks) {
			int i = listOfWorks.indexOf(element);
			worksDone += "\t" + stationDescription.getDescriptions().get(i).getWorkLabel().getText()+"\n";
			String[] data = stationDescription.getDescriptions().get(i).getDescriptionArea().getText().split("\n");
			for (String line : data) {
				worksDone += "\t\t" + line+"\n";
			}
			
			worksDone += "\n";
		}
		String worksToDo="";
		if (!stationDescription.getCbStationIsOk().isSelected()) {
			worksToDo += "\t" + stationDescription.getLblToDo().getText() + "\n";
			String[] data = stationDescription.getTaWorksToDo().getText().split("\n");
			for (String line : data) {
				worksToDo += "\t\t" + line+"\n";
			}
		}
		
		status = 	stationName + ": "+ stationStatus + "\n\n"+
					worksDone +
					worksToDo + "\n" +
					replacedWearingParts + "\n"+
					temp;

		return status;
	}

	protected boolean clearTempFile() {	
		PrintWriter writer;
		try {
			File tempFile = new File(getShiftReportTemp());
			writer = new PrintWriter(tempFile);
			writer.print("");
			writer.close();
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}


}





log;k:\Minosegbiztositas\25) Ipari méréstechnika\01_Mérőgépek\01_AMEST\01_Karbantartási előzmények\DQ250K OP100-200 KS-686-1_beavatkozási napló.xls
wearingPartsLog;n:\01-Merogepek\1-Amest\DQ250K\OP100-200 KS686-1\KS686-1_DQ250K_P100-200_WearingPartsLog.txt
[WearingParts]
Alsó Fix Ball
Alsó Spring Ball
Alsó Csapágyak
Felső Fix Ball
Felső Spring Ball
Felső Csapágyak
Motor
T1-tapinto
T2-tapinto
T3-tapinto
T4-tapinto
T5-tapinto
T6-tapinto
T7-tapinto
T8-tapinto
T9-tapinto
T10-tapinto
T11-tapinto
T12-tapinto
T13-tapinto
T14-tapinto
T15-tapinto
T16-tapinto
T17-tapinto
T18-tapinto
T19-tapinto
T20-tapinto
Dinamikus fej
[/WearingParts]
[KnownProblems]
Alkatrész megszorul		
Alkatrész forgása lassul		
Alkatrészt nem viszi be		
Alkatrészt nem hozza ki		
Alkatrészt karcolja a mérőgép		
M1 konstans eltérés ref-től 		
M1 ismétlőképesség hiba		
M2 konstans eltérés ref-től 		
M2 ismétlőképesség hiba		
M3-1 konstans eltérés ref-től 		
M3-1 ismétlőképesség hiba		
M3-2 konstans eltérés ref-től		
M3-2 ismétlőképesség hiba		
M4 konstans eltérés ref-től		
M4 ismétlőképesség hiba		
M5-1 konstans eltérés ref-től		
M5-1 ismétlőképesség hiba		
M5-2 konstans eltérés ref-től		
M5-2 ismétlőképesség hiba		
M6 konstans eltérés ref-től		
M6 ismétlőképesség hiba		
M7 konstans eltérés ref-től		
M7 ismétlőképesség hiba		
M8 konstans eltérés ref-től		
M8 ismétlőképesség hiba		
M9 konstans eltérés ref-től		
M9 ismétlőképesség hiba		
M10-1 konstans eltérés ref-től		
M10-1 ismétlőképesség hiba		
M10-2 konstans eltérés ref-től		
M10-2 ismétlőképesség hiba		
M11 konstans eltérés ref-től		
M11 ismétlőképesség hiba		
M12 konstans eltérés ref-től		
M12 ismétlőképesség hiba		
M13 konstans eltérés ref-től		
M13 ismétlőképesség hiba		
M14-1 konstans eltérés ref-től		
M14-1 ismétlőképesség hiba		
M14-2 konstans eltérés ref-től		
M14-2 ismétlőképesség hiba			
[/KnownProblems]